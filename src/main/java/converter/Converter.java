package converter;

public class Converter {

    public double getValue(String temp){
        return Double.parseDouble(temp) * 1.8 +32;
    }
}
